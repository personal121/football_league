FROM python:3.6-slim
RUN apt-get update
RUN apt-get install libgomp1
COPY ./ /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 80
ENTRYPOINT ["python", "./src/app.py"]
