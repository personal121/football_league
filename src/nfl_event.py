from providers.nfl import Nfl
import moment

nfl = Nfl()


class NflEvent:
    def _getEvents(self, startDate, endDate):
        '''
        returns all events between start date and end date

        @type startDate: String
        @param startDate: YYYY-MM-DD
        @type endDate: String
        @param endDate: YYYY-MM-DD
        @rtype events: List
        @return events: A list of all events in a scoreboard will be returned in a designed format
        '''
        try:
            events = []
            score = nfl.getScoreBoard(startDate, endDate)
            for (date, dateInfo) in score.items():
                if dateInfo:
                    dateData = dateInfo["data"]
                    for (eventId, eventData) in dateData.items():
                        event = {}
                        eventId = eventData["event_id"]
                        eventDate = moment.date(date).format("DD-MM-YYYY")
                        eventTime = moment.date(eventData["event_date"]).format("HH:mm")
                        awayTeamId = eventData['away_team_id']
                        awayNickname = eventData['away_nick_name']
                        awayCity = eventData['away_city']
                        homeTeamId = eventData['home_team_id']
                        homeNickname = eventData['home_nick_name']
                        homeCity = eventData['home_city']

                        event['event_id'] = eventId
                        event['event_date'] = eventDate
                        event['event_time'] = eventTime
                        event['away_team_id'] = awayTeamId
                        event['away_nick_name'] = awayNickname
                        event['away_city'] = awayCity
                        event['home_team_id'] = homeTeamId
                        event['home_nick_name'] = homeNickname
                        event['home_city'] = homeCity

                        events.append(event)

            return events
        except Exception as ex:
            raise Exception(ex)

    def _getEventTeamsRanking(self, eventTeams):
        '''
        returns rank and score of teams of an event

        @type eventTeams: Dict
        @param eventTeams: Team ids of an event
        @rtype result: Dict
        @return result: Rank and score of teams will be returned
        '''
        try:
            result = {}
            rankings = nfl.getTeamRankings()
            teams = rankings["data"] if rankings else raise Exception("There's no last updated rankings")
            for team in teams:
                if eventTeams['awayTeamId'] == team['team_id']:
                    result['away_rank'] = team['rank']
                    result['away_rank_points'] = str(round(float(team['adjusted_points']), 2))

                if eventTeams['homeTeamId'] == team['team_id']:
                    result['home_rank'] = team['rank']
                    result['home_rank_points'] = str(round(float(team['adjusted_points']), 2))

            return result
        except Exception as ex:
            raise Exception(ex)

    def getCombinedResult(self, startDate, endDate):
        '''
        returns combined result of scoreboard and team rankings between start date and end date


        @type startDate: String
        @param startDate: YYYY-MM-DD
        @type endDate: String
        @param endDate: YYYY-MM-DD
        @rtype events: List
        @return events: A combined result of all events in a scoreboard and team rankings will be returned in a designed format
        '''
        try:
            global nfl
            result = []
            events = self._getEvents(startDate, endDate)
            for event in events:
                eventTeams = {
                    "awayTeamId": event["away_team_id"],
                    "homeTeamId": event["home_team_id"]
                }
                teamsRank = self._getEventTeamsRanking(eventTeams)
                fullEvent = {**event, **teamsRank}
                result.append(fullEvent)

            return result
        except Exception as ex:
            raise Exception(ex)

# print(getResult("2020-01-12", "2020-01-19"))
