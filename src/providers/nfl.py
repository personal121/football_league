import json
import requests


class Nfl:

    def getScoreBoard(self, startDate, endDate):
        '''
        Return the scoreboard of all the events happened a start date and end date


        @type startDate: String
        @param startDate: YYYY-MM-DD
        @type endDate: String
        @param endDate: YYYY-MM-DD
        @rtype events: Dict
        @return events: An object mapping all events to certain dates
        '''
        try:
            url = f"https://delivery.chalk247.com/scoreboard/NFL/{startDate}/{endDate}.json"
            headers = {'Content-Type': 'application/json'}
            response = requests.get(url, params={"api_key": "74db8efa2a6db279393b433d97c2bc843f8e32b0"}, headers=headers)
            data = json.loads(response.content)["results"]
            return data
        except Exception as ex:
            raise Exception("Failed to get score board", ex)

    def getTeamRankings(self):
        '''
        Return the last updated team rankings and scores of all teams


        @rtype events: List
        @return events: A list of all teams with their rankings and scores 
        '''
        try:
            url = "https://delivery.chalk247.com/team_rankings/NFL.json"
            headers = {'Content-Type': 'application/json'}
            response = requests.get(url, params={"api_key": "74db8efa2a6db279393b433d97c2bc843f8e32b0"}, headers=headers)
            data = json.loads(response.content)["results"]
            return data
        except Exception as ex:
            raise Exception("Failed to get team rankings", ex)
