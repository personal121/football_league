from flask import Flask
from flask import request
from flask_swagger_ui import get_swaggerui_blueprint
from nfl_event import NflEvent
import moment

nflEvent = NflEvent()
app = Flask(__name__)

### swagger specific ###
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Football league"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
### end swagger specific ###

def dateValidation(startDate, endDate):
    '''
    checks end date is greater than or equal to start date
    return boolean

    @type startDate: String
    @param startDate: YYYY-MM-DD
    @type endDate: String
    @param endDate: YYYY-MM-DD
    @rtype result: Boolean
    @return result: returns True if validation is proper
    '''
    try:
        if moment.utc(endDate).diff(moment.utc(startDate), 'days').days >= 0:
            return True
    except Exception as ex:
        print(ex)
        return False


@app.route("/events/", methods=['GET'])
def get():
    try:
        global nflEvent
        startDate = ''
        endDate = ''
        if 'startDate' in request.args:
            startDate = request.args['startDate']
        if 'endDate' in request.args:
            endDate = request.args['endDate']

        if dateValidation(startDate, endDate):
            result = nflEvent.getCombinedResult(startDate, endDate)
        else:
            return {"error": "End date should be greater than or equal to start date"}
        return {"result": result}
    except Exception as ex:
        print(ex)
        return {"error": "Failed"}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
