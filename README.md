# Football_league

This project runs a web service and exposes an endpoint that takes the start date and end date as input parameters, combines responses of below input endpoints for each event and return all the events


- **Input:**
1. **Scoreboard**: /scoreboard/{league}/{start_date}/{end_date}
1. **Team Rankings**: /team_rankings/{league}


- **Output:**

1. **Events**: /events/?startDate={startDate}&endDate={enddate}


# Running to view the outcome of project


Approach 1: Deployed in AWS EC2
* **Endpoint**: http://ec2-99-79-7-111.ca-central-1.compute.amazonaws.com/swagger/


Approach 2: Steps to run the web service in your linux system
1. Install docker-compose in your linux system
2. Clone the repository (git clone git@gitlab.com:personal121/football_league.git)
3. Run command ->  docker-compose up -d (This command pulls the registry image from the gitlab and runs the docker container)
4. Find out ip address of your system (command -> ifconfig)
5. Hit the address http://ip-address/swagger in your browser, it displays the Swagger UI of web service.
6. Test the API endpoint by providing input startDate & endDate parameters

